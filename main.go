package main

import (
	"fmt"
	"log"
	"os"
	"user_admin/handlers"
	"user_admin/models"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/streadway/amqp"
)

func main() {

	host := os.Getenv("DB_HOST")
	rabbitMQURL := os.Getenv("AMQP_URL")
	dbUser := os.Getenv("DB_USER")
	dbName := os.Getenv("DB_NAME")
	dbPassword := os.Getenv("DB_PASSWORD")

	conn, err := amqp.Dial(rabbitMQURL)
	if err != nil {
		log.Fatal("Error al conectar a RabbitMQ:", err)
	}
	defer conn.Close()

	models.InitDB(dbUser, dbPassword, dbName, host)
	models.Migrate()

	r := gin.Default()
	config := cors.DefaultConfig()
	config.AllowOrigins = []string{"http://localhost:5173"}
	config.AllowMethods = []string{"GET", "POST", "PUT", "DELETE"}
	r.Use(cors.New(config))

	userRouter := r.Group("/user")
	{
		userRouter.POST("/register", func(c *gin.Context) {
			message := fmt.Sprintf("Usuario con ID %s se registró", c.Param("id"))
			publishToRabbitMQ(conn, message)
			handlers.Register(c)
		})
		userRouter.POST("/login", func(c *gin.Context) {
			message := fmt.Sprintf("Usuario con ID %s se logeo", c.Param("id"))
			publishToRabbitMQ(conn, message)
			handlers.Login(c)
		})
		userRouter.PATCH("/update", handlers.UpdateUser)
		userRouter.DELETE("/delete", handlers.DeleteUser)
		userRouter.GET("/all", handlers.GetUsers)
		userRouter.GET("/user/:id", handlers.GetUser)
		userRouter.GET("/isadmin/:id", func(c *gin.Context) {
			message := fmt.Sprintf("Usuario con ID %s verificó permisos de administrador", c.Param("id"))
			publishToRabbitMQ(conn, message)
			handlers.IsUserAdmin(c)
		})
	}

	r.Run(":8081")
}

func publishToRabbitMQ(conn *amqp.Connection, message string) {
	ch, err := conn.Channel()
	if err != nil {
		log.Println("Error al crear el canal RabbitMQ:", err)
		return
	}
	defer ch.Close()

	queue, err := ch.QueueDeclare(
		"usuario-admin-logs",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Println("Error al declarar la cola RabbitMQ:", err)
		return
	}

	err = ch.Publish(
		"",
		queue.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(message),
		},
	)
	if err != nil {
		log.Println("Error al publicar en RabbitMQ:", err)
		return
	}

	fmt.Printf("Mensaje publicado en RabbitMQ: %s\n", message)
}
