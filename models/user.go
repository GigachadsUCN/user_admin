package models

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type User struct {
	gorm.Model
	Username string
	Email    string `gorm:"unique"`
	Password string
	IsAdmin  bool `gorm:"type:bool;default:false"`
}

var DB *gorm.DB

func Migrate() {
	DB.AutoMigrate(&User{})
}
func InitDB(user, password, dbname, dbhost string) {
	var err error
	if user == "" {
		user = os.Getenv("DB_USER")
	}
	if password == "" {
		password = os.Getenv("DB_PASSWORD")
	}
	if dbname == "" {
		dbname = os.Getenv("DB_NAME")
	}
	if dbhost == "" {
		dbhost = os.Getenv("DB_HOST")
	}

	dbConnectionStr := fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable password=%s", dbhost, user, dbname, password)

	DB, err = gorm.Open("postgres", dbConnectionStr)
	if err != nil {
		panic("Error al conectar a la base de datos: " + err.Error())
	}
}
